- What kind of architecture did you use?
  Answer : I'M Use Flutter with architecture Model, View, Router, Bloc, RxDart.

- What libraries did you add to the app? What are they used for? if you not rely the other library tell us why?
  Answer :  
  cupertino_icons = > for Icons IOS
  mvc_pattern => Pattern MVC
  rxdart => RxDart for Stream
  google_fonts => Font for Google
  url_launcher => for Open Apps, Example Open Whatsapp Chat
  font_awesome_flutter => Font Awesome Icon For Flutter, because there is missing icons, example whatsapp

- How long did you spend on the test?
  Answer : 2,8 Days

- if you had more time, what further improvements or new features would you add?
  Answer : Maybe, Fitur Searching and animation for smooting display screen.

- Which parts of your submission are you most proud of? And why? little advice, dont answer whole project, use one what you think it special.
  Answer :
  Framework Flutter, Because publisher google and this framework can hybrid Code for, Android, IOS, Web, Desktop.
  Router Lib Flutter, easy use for jump one page into next page.
  In Flutter Many Library and easy for installation.

- Which parts did you spend the most time with? What did you find most difficult?
  Answer :
  Create first structure, before development.
  Search Object, Merge Object / Array in frontend mobile, maybe easier on the backend part.

- How did you find the test overall? If you have any suggestions on how we can improve the test or our API, we'd love to hear them, please elaborate more about this.
  Answer :
  Mau Kerja App Cannot Login, Rest Api History Double im track with httpcanary sample api => api:443api:443, and slow response for get api. maybe can stronger for backend data for compatibility server.

- Last question, quality or quantity?
  Answer :
  Quality and then quantity. because for the first impression for users, friendly, easy to use, and few bugs.