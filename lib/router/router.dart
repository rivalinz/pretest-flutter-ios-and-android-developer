import 'package:flutter/material.dart';
import 'package:flutter_pretest/src/candidates_ds.dart';
import 'package:flutter_pretest/ui/pages/blog_details_screen.dart';
import 'package:flutter_pretest/ui/pages/candidates_details_screen.dart';
import 'package:flutter_pretest/ui/pages/home_screen.dart';

class Routers {
  static Generate(settings) {
    switch (settings.name) {
      case '/candidates_details':
        return MaterialPageRoute(builder: (context) {
          return CandidatesDetails(
            photoUrl: settings.arguments["photoUrl"],
            fullname: settings.arguments["fullname"],
            gender: settings.arguments["gender"],
            birthday: settings.arguments["birthday"],
            expired: settings.arguments["expired"],
            email: settings.arguments["email"],
            phone: settings.arguments["phone"],
            address: settings.arguments["address"],
            city: settings.arguments["city"],
            state: settings.arguments["state"],
            zip_code: settings.arguments["zip_code"],
            status: settings.arguments["status"],
            job_title: settings.arguments["job_title"],
            company_name: settings.arguments["company_name"],
            industry: settings.arguments["industry"],
          );
        });
        break;

      case '/blog_details':
        return MaterialPageRoute(builder: (context) {
          return BlogDetails(
            api: settings.arguments["api"],
            publish: settings.arguments["publish"],
            author: settings.arguments["author"],
            desc: settings.arguments["desc"],
            imgUrl: settings.arguments["imgUrl"],
            content: settings.arguments["content"],
            title: settings.arguments["title"],
            tag: settings.arguments["tag"],
          );
        });
        break;
    }
  }
}
