import 'dart:async';
import 'package:rxdart/rxdart.dart';
import 'package:flutter_pretest/src/candidates_ds.dart';
import 'package:flutter_pretest/bloc/candidates_state.dart';

class CandidatesBloc {
  Stream<CandidatesState> search(String term, CandidatesApi api) {
    return Rx.fromCallable(() => api.search(term))
        .map((result) =>
    result.isEmpty ? CandidatesEmpty() : CandidatesPopulated(result))
        .startWith(CandidatesLoading())
        .onErrorReturn(CandidatesError());
    // List<String> dummySearchList = List<String>();
    // dummySearchList.addAll(duplicateItems);
    // if(query.isNotEmpty) {
    //   List<String> dummyListData = List<String>();
    //   dummySearchList.forEach((item) {
    //     if(item.contains(query)) {
    //       dummyListData.add(item);
    //     }
    //   });
    //   setState(() {
    //     items.clear();
    //     items.addAll(dummyListData);
    //   });
    //   return;
    // } else {
    //   setState(() {
    //     items.clear();
    //     items.addAll(duplicateItems);
    //   });
    // }
    // =>
    //     Rx.fromCallable(() => api.search(term))
    //         .map((result) =>
    //     result.isEmpty ? CandidatesEmpty() : CandidatesPopulated(result))
    //         .startWith(CandidatesLoading())
    //         .onErrorReturn(CandidatesError());
  }

  Stream<CandidatesState> getCandidates(CandidatesApi api) =>
      Rx.fromCallable(() => api.getData())
          .map((result) =>
              result.isEmpty ? CandidatesEmpty() : CandidatesPopulated(result))
          .startWith(CandidatesLoading())
          .onErrorReturn(CandidatesError());
}
