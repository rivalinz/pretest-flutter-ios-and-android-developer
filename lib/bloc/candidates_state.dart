import 'package:flutter_pretest/src/candidates_ds.dart';

// The State represents the data the View requires. The View consumes a Stream
// of States. The view rebuilds every time the Stream emits a new State!
//
// The State Stream will emit new States depending on the situation: The
// initial state, loading states, the list of results, and any errors that
// happen.
//
// The State Stream responds to input from the View by accepting a
// Stream<String>. We call this Stream the onTextChanged "intent".
class CandidatesState {}

class CandidatesLoading extends CandidatesState {}

class CandidatesError extends CandidatesState {}

class CandidatesNoTerm extends CandidatesState {}

class CandidatesPopulated extends CandidatesState {
  final CandidatesResult result;

  CandidatesPopulated(this.result);
}

class CandidatesEmpty extends CandidatesState {}
