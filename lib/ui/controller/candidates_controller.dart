import 'package:flutter_pretest/model/candidates.dart';
import 'package:flutter_pretest/src/candidates_ds.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class PostsController extends ControllerMVC {
  late CandidatesDs postsDs;
  List<Candidates> posts = [];

  static final PostsController _controller = PostsController._internal();

  factory PostsController() {
    _controller.postsDs = CandidatesDs();
    return _controller;
  }

  PostsController._internal();

  void getPosts() async {
    posts = await postsDs.getPosts();
    setState(() {});
  }
}
