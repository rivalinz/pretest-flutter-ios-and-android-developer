import 'package:flutter_pretest/model/candidates.dart';
import 'package:flutter_pretest/src/candidates_ds.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class CandidatesController extends ControllerMVC {
  late CandidatesDs postsDs;
  List<Candidates> posts = [];

  static final CandidatesController _controller = CandidatesController._internal();

  factory CandidatesController() {
    _controller.postsDs = CandidatesDs();
    return _controller;
  }

  CandidatesController._internal();

  void getCandidates() async {
    posts = await postsDs.getPosts();
    setState(() {});
  }
}
