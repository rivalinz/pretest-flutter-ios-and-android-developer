import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_pretest/src/candidates_ds.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:url_launcher/url_launcher.dart';

class CandidatesDetails extends StatelessWidget {
  late final String photoUrl,
      fullname,
      gender,
      birthday,
      expired,
      email,
      phone,
      address,
      city,
      state,
      status,
      job_title,
      company_name,
      industry;
  late final int zip_code;
  late BuildContext context;

  late final CandidatesApi api;

  CandidatesDetails({
    Key? key,
    required this.photoUrl,
    required this.fullname,
    required this.gender,
    required this.birthday,
    required this.expired,
    required this.email,
    required this.phone,
    required this.address,
    required this.city,
    required this.state,
    required this.status,
    required this.job_title,
    required this.company_name,
    required this.industry,
    required this.zip_code,
  }) : super(key: key);

  openwhatsapp(number) async {
    var whatsapp = number;
    var whatsappURl_android =
        "whatsapp://send?phone=" + whatsapp + "&text=Hi i am MK company";
    var whatappURL_ios =
        "https://wa.me/$whatsapp?text=${Uri.parse("Hi i am MK company")}";
    if (Platform.isIOS) {
      // for iOS phone only
      if (await canLaunch(whatappURL_ios)) {
        await launch(whatappURL_ios, forceSafariVC: false);
      } else {
        ScaffoldMessenger.of(context)
            .showSnackBar(SnackBar(content: new Text("whatsapp no installed")));
      }
    } else {
      // android , web
      if (await canLaunch(whatsappURl_android)) {
        await launch(whatsappURl_android);
      } else {
        ScaffoldMessenger.of(context)
            .showSnackBar(SnackBar(content: new Text("whatsapp no installed")));
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Candidates Details'),
        ),
        body: ListView(
          children: [
            Container(
                margin: const EdgeInsets.only(top: 24, bottom: 24),
                width: MediaQuery.of(context).size.width,
                child: Container(
                  padding: const EdgeInsets.symmetric(horizontal: 16),
                  alignment: Alignment.topLeft,
                  decoration: const BoxDecoration(
                      borderRadius: BorderRadius.only(
                          bottomRight: Radius.circular(6),
                          bottomLeft: Radius.circular(6))),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      ClipRRect(
                          borderRadius: BorderRadius.circular(6),
                          child: Image.network(
                            "${photoUrl}",
                            width: MediaQuery.of(context).size.width,
                            fit: BoxFit.cover,
                          )),
                      const SizedBox(
                        height: 12,
                      ),
                      const Text(
                        "Info Profile :",
                        maxLines: 2,
                        style: TextStyle(
                            color: Colors.black87,
                            fontSize: 20,
                            fontWeight: FontWeight.w500),
                      ),
                      const Divider(
                        height: 8,
                        thickness: 2,
                      ),
                      Text(
                        "Fullname: ${fullname}",
                        style: const TextStyle(
                            color: Colors.black54, fontSize: 14),
                      ),
                      const SizedBox(
                        height: 4,
                      ),
                      Text(
                        "Gender: ${gender}",
                        style: const TextStyle(
                            color: Colors.black54, fontSize: 14),
                      ),
                      const SizedBox(
                        height: 4,
                      ),
                      Text(
                        "Birthday: ${birthday}",
                        style: const TextStyle(
                            color: Colors.black54, fontSize: 14),
                      ),
                      const SizedBox(
                        height: 4,
                      ),
                      Text(
                        "Expired Date: ${expired}",
                        style: const TextStyle(
                            color: Colors.black54, fontSize: 14),
                      ),
                      const SizedBox(
                        height: 16,
                      ),

                      const Text(
                        "Address :",
                        maxLines: 2,
                        style: TextStyle(
                            color: Colors.black87,
                            fontSize: 20,
                            fontWeight: FontWeight.w500),
                      ),
                      const Divider(
                        height: 8,
                        thickness: 2,
                      ),
                      Text(
                        "Address: ${address}",
                        style: const TextStyle(
                            color: Colors.black54, fontSize: 14),
                      ),
                      const SizedBox(
                        height: 4,
                      ),
                      Text(
                        "City: ${city}",
                        style: const TextStyle(
                            color: Colors.black54, fontSize: 14),
                      ),
                      const SizedBox(
                        height: 4,
                      ),
                      Text(
                        "State: ${state}",
                        style: const TextStyle(
                            color: Colors.black54, fontSize: 14),
                      ),
                      const SizedBox(
                        height: 4,
                      ),
                      Text(
                        "Zip Code: ${zip_code}",
                        style: const TextStyle(
                            color: Colors.black54, fontSize: 14),
                      ),
                      const SizedBox(
                        height: 16,
                      ),
                      const Text(
                        "Contact :",
                        maxLines: 2,
                        style: TextStyle(
                            color: Colors.black87,
                            fontSize: 20,
                            fontWeight: FontWeight.w500),
                      ),
                      const Divider(
                        height: 8,
                        thickness: 2,
                      ),
                      Text(
                        "Email: ${email}",
                        style: const TextStyle(
                            color: Colors.black54, fontSize: 14),
                      ),
                      const SizedBox(
                        height: 4,
                      ),
                      Text(
                        "Phone Number: ${phone}",
                        style: const TextStyle(
                            color: Colors.black54, fontSize: 14),
                      ),
                      const SizedBox(
                        height: 16,
                      ),
                      const Text(
                        "Experience :",
                        maxLines: 2,
                        style: TextStyle(
                            color: Colors.black87,
                            fontSize: 20,
                            fontWeight: FontWeight.w500),
                      ),
                      const Divider(
                        height: 8,
                        thickness: 2,
                      ),
                      Text(
                        "Company Name: ${company_name}",
                        style: const TextStyle(
                            color: Colors.black54, fontSize: 14),
                      ),
                      const SizedBox(
                        height: 4,
                      ),
                      Text(
                        "Industry: ${industry}",
                        style: const TextStyle(
                            color: Colors.black54, fontSize: 14),
                      ),
                      const SizedBox(
                        height: 4,
                      ),
                      Text(
                        "Status: ${status}",
                        style: const TextStyle(
                            color: Colors.black54, fontSize: 14),
                      ),
                      const SizedBox(
                        height: 16,
                      ),
                      const Text(
                        "Direct Contact :",
                        maxLines: 2,
                        style: TextStyle(
                            color: Colors.black87,
                            fontSize: 20,
                            fontWeight: FontWeight.w500),
                      ),
                      const Divider(
                        height: 8,
                        thickness: 2,
                      ),
                      const SizedBox(
                        height: 4,
                      ),
                      FloatingActionButton(
                        child: const FaIcon(FontAwesomeIcons.whatsapp),
                        onPressed: () => openwhatsapp(phone.replaceAll('-', '').replaceAll(' ', '')),
                        backgroundColor: Colors.green.shade800,
                      )
                    ],
                  ),
                ))
          ],
        ));
    ;
  }
}
