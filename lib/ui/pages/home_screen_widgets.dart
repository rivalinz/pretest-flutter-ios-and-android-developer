import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_pretest/model/candidates.dart';
import 'package:flutter_pretest/src/candidates_ds.dart';
import 'package:intl/intl.dart';

class LoadingWidget extends StatelessWidget {
  final Color color;

  const LoadingWidget({
    Key? key,
    required this.color,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: CircularProgressIndicator(
        backgroundColor: color,
      ),
    );
  }
}

class InfoWidget extends StatelessWidget {
  final String text;

  const InfoWidget({
    Key? key,
    required this.text,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text(
        text,
        style: const TextStyle(
          color: Colors.black,
          fontSize: 32,
        ),
      ),
    );
  }
}

class HomeItemWidget extends StatelessWidget {
  final Candidates post;

  const HomeItemWidget({Key? key, required this.post}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(post.id.toString()),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              post.name ?? 'No title',
              style: const TextStyle(fontSize: 20.0, color: Colors.black),
              textAlign: TextAlign.center,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              post.photo ?? 'No body',
              style: const TextStyle(fontSize: 16.0, color: Colors.blueGrey),
            ),
          ),
        ],
      ),
    );
  }
}

class CandidatesErrorWidget extends StatelessWidget {
  const CandidatesErrorWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: FractionalOffset.center,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Icon(Icons.error_outline, color: Colors.red[300], size: 80.0),
          Container(
            padding: const EdgeInsets.only(top: 16.0),
            child: Text(
              'Oops, something went wrong!',
              style: TextStyle(
                color: Colors.red[300],
              ),
            ),
          )
        ],
      ),
    );
  }
}

class EmptyWidget extends StatelessWidget {
  const EmptyWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: FractionalOffset.center,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Icon(
            Icons.warning,
            color: Colors.yellow[200],
            size: 80.0,
          ),
          Container(
            padding: const EdgeInsets.only(top: 16.0),
            child: Text(
              'No results',
              style: TextStyle(color: Colors.yellow[100]),
            ),
          )
        ],
      ),
    );
  }
}

class CandidatesIntro extends StatelessWidget {
  const CandidatesIntro({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: FractionalOffset.center,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Icon(Icons.info, color: Colors.green[200], size: 80.0),
          Container(
            padding: const EdgeInsets.only(top: 16.0),
            child: Text(
              'Enter a search term to begin',
              style: TextStyle(
                color: Colors.green[100],
              ),
            ),
          )
        ],
      ),
    );
  }
}

class CandidatesResultWidget extends StatelessWidget {
  final List<CandidatesResultItem> items;
  final CandidatesApi api;

  const CandidatesResultWidget(
      {Key? key, required this.api, required this.items})
      : super(key: key);

  _FormatExpiredDate(timeInMillis) {
    DateTime date = DateTime.fromMillisecondsSinceEpoch(timeInMillis * 1000);
    return DateFormat.yMMMd().format(date); // Apr 8, 2020
  }

  _FormatBirthDate(timeInMillis) {
    DateTime date = DateTime.fromMillisecondsSinceEpoch(timeInMillis);
    return DateFormat.yMMMd().format(date); // Apr 8, 2020
  }

  static String utf8convert(String text) {
    List<int> bytes = text.toString().codeUnits;
    return utf8.decode(bytes);
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: items.length,
      itemBuilder: (context, index) {
        final item = items[index];
        if (item.type == 'candidates') {
          return CandidatesTile(
            api: api,
            birthday: _FormatBirthDate(item.birthday),
            expired: _FormatExpiredDate(item.expired),
            gender: item.gender == 'm' ? 'Male' : 'Female',
            name: utf8convert(item.name),
            photo: utf8convert(item.photo),
            address: utf8convert(item.address),
            city: utf8convert(item.city),
            company_name: utf8convert(item.company_name),
            email: utf8convert(item.email),
            industry: utf8convert(item.industry),
            job_title: utf8convert(item.job_title),
            phone: utf8convert(item.phone),
            state: utf8convert(item.state),
            status: utf8convert(item.status),
            zip_code: item.zip_code,
          );
        } else if (item.type == 'blogs') {
          return BlogsTile(
              title: utf8convert(item.title),
              content: utf8convert(item.subTitle),
              imgUrl: item.photo,
              desc: utf8convert(item.content),
              author: utf8convert(item.author),
              publish: _FormatExpiredDate(item.create_at),
              tag: utf8convert(item.tag),
              api: api);
        }
        return Container();
      },
    );
  }

  void showItem(BuildContext context, CandidatesResultItem item) {
    Navigator.push(
      context,
      MaterialPageRoute<void>(
        builder: (BuildContext context) {
          return Scaffold(
            resizeToAvoidBottomInset: false,
            body: GestureDetector(
              key: Key(item.photo),
              onTap: () => Navigator.pop(context),
              child: SizedBox.expand(
                child: Hero(
                  tag: item.name,
                  child: Image.network(
                    item.photo,
                    width: MediaQuery.of(context).size.width,
                    height: 300.0,
                  ),
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}

class BlogsTile extends StatelessWidget {
  late final String imgUrl, title, desc, content, publish, author, tag;
  late final CandidatesApi api;

  BlogsTile(
      {Key? key,
      required this.imgUrl,
      required this.desc,
      required this.title,
      required this.content,
      required this.publish,
      required this.author,
      required this.tag,
      required this.api})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => Navigator.pushNamed(context, "/blog_details", arguments: {
        "api": api,
        "imgUrl": imgUrl,
        "desc": desc,
        "title": title,
        "content": content,
        "publish": publish,
        "author": author,
        "tag": tag,
      }),
      child: Container(
          margin: const EdgeInsets.only(top: 8, bottom: 14),
          width: MediaQuery.of(context).size.width,
          child: Container(
            padding: const EdgeInsets.symmetric(horizontal: 16),
            alignment: Alignment.bottomCenter,
            decoration: const BoxDecoration(
                borderRadius: BorderRadius.only(
                    bottomRight: Radius.circular(6),
                    bottomLeft: Radius.circular(6))),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                ClipRRect(
                    borderRadius: BorderRadius.circular(6),
                    child: Image.network(
                      imgUrl,
                      height: 200,
                      width: MediaQuery.of(context).size.width,
                      fit: BoxFit.cover,
                    )),
                const SizedBox(
                  height: 12,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Publish at: ${publish}',
                      maxLines: 2,
                      style: const TextStyle(
                          color: Colors.black38,
                          fontSize: 14,
                          fontWeight: FontWeight.w500),
                    ),
                    Text(
                      'Creator by: ${author}',
                      maxLines: 2,
                      style: const TextStyle(
                          color: Colors.black38,
                          fontSize: 14,
                          fontWeight: FontWeight.w500),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 12,
                ),
                Text(
                  title,
                  maxLines: 2,
                  style: const TextStyle(
                      color: Colors.black87,
                      fontSize: 20,
                      fontWeight: FontWeight.w500),
                ),
                const SizedBox(
                  height: 4,
                ),
                Text(
                  content,
                  style: const TextStyle(color: Colors.black54, fontSize: 14),
                ),
                const SizedBox(
                  height: 10,
                ),
                Text('Tag: ${tag}',
                    style: const TextStyle(
                        color: Colors.blueAccent,
                        fontSize: 14,
                        fontWeight: FontWeight.w800))
              ],
            ),
          )),
    );
  }
}

class CandidatesTile extends StatelessWidget {
  late final String photo,
      name,
      gender,
      birthday,
      expired,
      email,
      phone,
      address,
      city,
      state,
      status,
      job_title,
      company_name,
      industry;
  late final int zip_code;
  late final CandidatesApi api;

  CandidatesTile(
      {Key? key,
      required this.photo,
      required this.name,
      required this.gender,
      required this.birthday,
      required this.expired,
      required this.email,
      required this.phone,
      required this.address,
      required this.city,
      required this.state,
      required this.status,
      required this.job_title,
      required this.company_name,
      required this.industry,
      required this.zip_code,
      required this.api})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () =>
          Navigator.pushNamed(context, "/candidates_details", arguments: {
        "api": api,
        "photoUrl": photo,
        "fullname": name,
        "gender": gender,
        "birthday": birthday,
        "expired": expired,
        "email": email,
        "phone": phone,
        "address": address,
        "city": city,
        "state": state,
        "zip_code": zip_code,
        "status": status,
        "job_title": job_title,
        "company_name": company_name,
        "industry": industry,
      }),
      child: Container(
        alignment: FractionalOffset.center,
        margin: const EdgeInsets.fromLTRB(16.0, 12.0, 16.0, 12.0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              margin: const EdgeInsets.only(right: 16.0),
              child: Hero(
                tag: '${name}',
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(6),
                  child: Image.network(
                    photo,
                    width: 56.0,
                    height: 100.0,
                    fit: BoxFit.cover,
                    alignment: Alignment.center,
                  ),
                ),
              ),
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    margin: const EdgeInsets.only(
                      top: 6.0,
                      bottom: 4.0,
                    ),
                    child: Text(
                      '${name}',
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: const TextStyle(
                        fontFamily: 'Montserrat',
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  Text(
                    'Gender : ${(gender == 'm' ? 'Male' : 'Female')}',
                    style: const TextStyle(
                      fontFamily: 'Hind',
                    ),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
                  const Padding(padding: EdgeInsets.only(bottom: 4, top: 4)),
                  Text(
                    'Birthday : ${birthday}',
                    style: const TextStyle(
                      fontFamily: 'Hind',
                    ),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
                  const Padding(padding: EdgeInsets.only(bottom: 4, top: 4)),
                  Text(
                    'Expired : ${expired}',
                    style: const TextStyle(
                      fontFamily: 'Hind',
                    ),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
