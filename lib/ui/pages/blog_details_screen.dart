import 'package:flutter/material.dart';
import 'package:flutter_pretest/src/candidates_ds.dart';

class BlogDetails extends StatelessWidget {
  late final String imgUrl, title, desc, content, publish, author, tag;
  late final CandidatesApi api;

  BlogDetails(
      {Key? key,
      required this.imgUrl,
      required this.desc,
      required this.title,
      required this.content,
      required this.publish,
      required this.author,
      required this.tag,
      required this.api})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Blog Details'),
        ),
        body: ListView(
          children: [
            Container(
                margin: const EdgeInsets.only(top: 24, bottom: 24),
                width: MediaQuery.of(context).size.width,
                child: Container(
                  padding: const EdgeInsets.symmetric(horizontal: 16),
                  alignment: Alignment.topLeft,
                  decoration: const BoxDecoration(
                      borderRadius: BorderRadius.only(
                          bottomRight: Radius.circular(6),
                          bottomLeft: Radius.circular(6))),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      ClipRRect(
                          borderRadius: BorderRadius.circular(6),
                          child: Image.network(
                            imgUrl,
                            width: MediaQuery.of(context).size.width,
                            fit: BoxFit.cover,
                          )),
                      const SizedBox(
                        height: 12,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Publish at: ${publish}',
                            maxLines: 2,
                            style: const TextStyle(
                                color: Colors.black38,
                                fontSize: 14,
                                fontWeight: FontWeight.w500),
                          ),
                          Text(
                            'Creator by: ${author}',
                            maxLines: 2,
                            style: const TextStyle(
                                color: Colors.black38,
                                fontSize: 14,
                                fontWeight: FontWeight.w500),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 12,
                      ),
                      Text(
                        title,
                        maxLines: 2,
                        style: const TextStyle(
                            color: Colors.black87,
                            fontSize: 20,
                            fontWeight: FontWeight.w500),
                      ),
                      const SizedBox(
                        height: 4,
                      ),
                      Text(
                        desc,
                        style: const TextStyle(
                            color: Colors.black54, fontSize: 14),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Text('Tag: ${tag}',
                          style: const TextStyle(
                              color: Colors.blueAccent,
                              fontSize: 14,
                              fontWeight: FontWeight.w800))
                    ],
                  ),
                ))
          ],
        ));
  }
}
