import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_pretest/bloc/candidates_bloc.dart';
import 'package:flutter_pretest/bloc/candidates_state.dart';
import 'package:flutter_pretest/src/candidates_ds.dart';
import 'package:flutter_pretest/ui/controller/candidates_controller.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'home_screen_widgets.dart';

class HomeScreen extends StatefulWidget {
  final CandidatesApi api;

  const HomeScreen({Key? key, required this.api}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends StateMVC<HomeScreen> {
  // late PostsController _controller;
  late final CandidatesBloc bloc = CandidatesBloc();
  late bool search = false;
  late String TextSearch;

  // _HomeScreenState() : super(PostsController()) {
  //   _controller = PostsController();
  // }

  @override
  void initState() {
    super.initState();

    // _controller.getPosts();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return StreamBuilder<CandidatesState>(
      stream: search
          ? bloc.search(TextSearch, widget.api)
          : bloc.getCandidates(widget.api),
      initialData: CandidatesLoading(),
      builder: (BuildContext context, AsyncSnapshot<CandidatesState> snapshot) {
        final state = snapshot.requireData;

        return Scaffold(
          body: Stack(
            children: <Widget>[
              Flex(direction: Axis.vertical, children: <Widget>[
                Container(
                  padding: const EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 4.0),
                  child: Container(
                    padding: const EdgeInsets.only(left: 10.0, right: 10.0),
                    height: 45,
                    width: size.width,
                    decoration: BoxDecoration(
                        color: Colors.grey[100],
                        borderRadius: BorderRadius.circular(10.0)),
                    child: TextField(
                      onChanged: (str) {
                        setState(() {
                          search = str.length > 1 ? true : false;
                          TextSearch = str;
                        });
                      },
                      decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: 'Search Candidates & Blogs',
                          hintStyle: GoogleFonts.roboto(fontSize: 17),
                          suffixIcon: const Icon(Icons.search_rounded)),
                    ),
                  ),
                ),

                // TextField(
                //   decoration: const InputDecoration(
                //     border: InputBorder.none,
                //     hintText: 'Search Candidates & Blog...',
                //   ),
                //   style: const TextStyle(
                //     fontSize: 24.0,
                //     fontFamily: 'Hind',
                //     decoration: TextDecoration.none,
                //   ),
                //   onChanged: (String a) => a,
                // ),
                Expanded(
                  child: AnimatedSwitcher(
                    duration: const Duration(milliseconds: 300),
                    child: _buildChild(state),
                  ),
                )
              ])
            ],
          ),
        );
      },
    );
  }

  Widget _buildChild(CandidatesState state) {
    if (state is CandidatesNoTerm) {
      return const CandidatesIntro();
    } else if (state is CandidatesEmpty) {
      return const EmptyWidget();
    } else if (state is CandidatesLoading) {
      return const LoadingWidget(color: Colors.lightGreen);
    } else if (state is CandidatesError) {
      return const CandidatesErrorWidget();
    } else if (state is CandidatesPopulated) {
      return CandidatesResultWidget(api: widget.api, items: state.result.items);
    }

    throw Exception('${state.runtimeType} is not supported');
  }
}
