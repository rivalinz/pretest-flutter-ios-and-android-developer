import 'dart:convert';

class Blog {
  int? id;
  String? title;
  String? subTitle;
  String? photo;
  String? content;
  String? author;
  int? create_at;
  String? tag;

  Blog({
    this.id,
    this.title,
    this.subTitle,
    this.photo,
    this.content,
    this.author,
    this.create_at,
    this.tag,
  });

  Blog.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    subTitle = json['subTitle'];
    photo = json['photo'];
    content = json['content'];
    author = json['author'];
    create_at = json['create_at'];
    tag = json['tag'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['title'] = title;
    data['subTitle'] = subTitle;
    data['photo'] = photo;
    data['content'] = content;
    data['author'] = author;
    data['create_at'] = create_at;
    data['tag'] = tag;
    return data;
  }
}

List<Blog> blogListFromJson(String str) =>
    List<Blog>.from(json.decode(str)['results'].map((x) => Blog.fromJson(x)));

String blogListToJson(List<Blog> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));
