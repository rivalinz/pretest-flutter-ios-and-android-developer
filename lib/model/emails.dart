import 'dart:convert';

class Emails {
  int? id;
  String? email;
  String? phone;

  Emails({
    this.id,
    this.email,
    this.phone,
  });

  Emails.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    email = json['email'];
    phone = json['phone'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['email'] = email;
    data['phone'] = phone;
    return data;
  }
}

List<Emails> emailsListFromJson(String str) =>
    List<Emails>.from(json.decode(str)['results'].map((x) => Emails.fromJson(x)));

String emailsListToJson(List<Emails> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));
