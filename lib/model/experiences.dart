import 'dart:convert';

class Experiences {
  int? id;
  String? status;
  String? job_title;
  String? company_name;
  String? industry;

  Experiences({
    this.id,
    this.status,
    this.job_title,
    this.company_name,
    this.industry,
  });

  Experiences.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    status = json['status'];
    job_title = json['job_title'];
    company_name = json['company_name'];
    industry = json['industry'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['status'] = status;
    data['job_title'] = job_title;
    data['company_name'] = company_name;
    data['industry'] = industry;
    return data;
  }
}

List<Experiences> experiencesListFromJson(String str) =>
    List<Experiences>.from(json.decode(str)['results'].map((x) => Experiences.fromJson(x)));

String experiencesListToJson(List<Experiences> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));
