import 'dart:convert';

class Address {
  int? id;
  String? address;
  String? city;
  String? state;
  int? zip_code;

  Address({
    this.id,
    this.address,
    this.city,
    this.state,
    this.zip_code,
  });

  Address.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    address = json['address'];
    city = json['city'];
    state = json['state'];
    zip_code = json['zip_code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['address'] = address;
    data['city'] = city;
    data['state'] = state;
    data['zip_code'] = zip_code;
    return data;
  }
}

List<Address> AddressListFromJson(String str) =>
    List<Address>.from(json.decode(str)['results'].map((x) => Address.fromJson(x)));

String AddressListToJson(List<Address> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));
