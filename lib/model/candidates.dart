import 'dart:convert';

class Candidates {
  int? id;
  String? name;
  String? gender;
  String? photo;
  int? birthday;
  int? expired;

  Candidates({
    this.id,
    this.name,
    this.gender,
    this.photo,
    this.birthday,
    this.expired,
  });

  Candidates.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    gender = json['gender'];
    photo = json['photo'];
    birthday = json['birthday'];
    expired = json['expired'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    data['gender'] = gender;
    data['photo'] = photo;
    data['birthday'] = birthday;
    data['expired'] = expired;
    return data;
  }
}

class CandidatesResultItem {
  final int id;
  final String name;
  final String gender;
  final String photo;
  final int birthday;
  final int expired;
  final String title;
  final String subTitle;
  final String content;
  final String author;
  final int create_at;
  final String tag;
  final String type;

  final String email;
  final String phone;
  final String address;
  final String city;
  final String state;
  final int zip_code;
  final String status;
  final String job_title;
  final String company_name;
  final String industry;

  CandidatesResultItem(
      this.id,
      this.name,
      this.gender,
      this.photo,
      this.birthday,
      this.expired,
      this.title,
      this.subTitle,
      this.content,
      this.author,
      this.create_at,
      this.tag,
      this.type,
      this.email,
      this.phone,
      this.address,
      this.city,
      this.state,
      this.zip_code,
      this.status,
      this.job_title,
      this.company_name,
      this.industry,
      );

  factory CandidatesResultItem.fromJson(Map<String, dynamic> json) {
    return CandidatesResultItem(
        json['id'] as int,
        json['name'] == null ? '' : json['name'] as String,
        json['gender'] == null ? '' : json['gender'] as String,
        json['photo'] == null ? '' : json['photo'] as String,
        json['birthday'] == null ? 0 : json['birthday'] as int,
        json['expired'] == null ? 0 : json['expired'] as int,
        json['title'] == null ? '' : json['title'] as String,
        json['subTitle'] == null ? '' : json['subTitle'] as String,
        json['content'] == null ? '' : json['content'] as String,
        json['author'] == null ? '' : json['author'] as String,
        json['create_at'] == null ? 0 : json['create_at'] as int,
        json['tag'] == null ? '' : json['tag'] as String,
        json['subTitle'] == null ? 'candidates' : 'blogs',
        json['email'] == null ? '' : json['email'] as String,
        json['phone'] == null ? '' : json['phone'] as String,
        json['address'] == null ? '' : json['address'] as String,
        json['city'] == null ? '' : json['city'] as String,
        json['state'] == null ? '' : json['state'] as String,
        json['zip_code'] == null ? 0 : json['zip_code'] as int,
        json['status'] == null ? '' : json['status'] as String,
        json['job_title'] == null ? '' : json['job_title'] as String,
        json['company_name'] == null ? '' : json['company_name'] as String,
        json['industry'] == null ? '' : json['industry'] as String,
    );
  }
}

List<Candidates> candidatesListFromJson(String str) => List<Candidates>.from(
    json.decode(str)['results'].map((x) => Candidates.fromJson(x)));

String postsListToJson(List<Candidates> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));
