import 'package:flutter/material.dart';
import 'package:flutter_pretest/router/router.dart';
import 'package:flutter_pretest/src/candidates_ds.dart';
import 'package:flutter_pretest/ui/pages/home_screen.dart';

void main() {
  runApp(MyApp(api: CandidatesApi()));
}

class MyApp extends StatefulWidget {
  final CandidatesApi api;

  const MyApp({Key? key, required this.api}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  late final CandidatesApi api;

  @override
  void initState() {
    super.initState();

    api = widget.api;
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.deepPurple,
      ),
      home: Scaffold(
          appBar: AppBar(
            title: const Text('Home Screen'),
          ),
          body: HomeScreen(api: api)),
      onGenerateRoute: (settings) => Routers.Generate(settings),
    );
  }
}
