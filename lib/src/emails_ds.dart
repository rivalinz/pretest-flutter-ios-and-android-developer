import 'package:flutter_pretest/model/emails.dart';
import 'package:http/http.dart' as http;

class EmailsDs {
  final String _baseUrl = 'https://private-b9a758-candidattest.apiary-mock.com/';

  Future<List<Emails>> getEmails() async {
    List<Emails> emails = <Emails>[];

    final url = Uri.parse('${_baseUrl}/emails');

    http.Response response = await http.get(url);

    if (response.statusCode == 200) {
      emails.addAll(emailsListFromJson(response.body));
      return emails;
    } else {
      throw Exception("Unable to perform request!");
    }
  }
}
