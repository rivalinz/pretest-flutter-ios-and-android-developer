import 'package:flutter_pretest/model/experiences.dart';
import 'package:http/http.dart' as http;

class ExperiencesDs {
  final String _baseUrl = 'https://private-b9a758-candidattest.apiary-mock.com/';

  Future<List<Experiences>> getExperiences() async {
    List<Experiences> experiences = <Experiences>[];

    final url = Uri.parse('${_baseUrl}/experiences');

    http.Response response = await http.get(url);

    if (response.statusCode == 200) {
      experiences.addAll(experiencesListFromJson(response.body));
      return experiences;
    } else {
      throw Exception("Unable to perform request!");
    }
  }
}
