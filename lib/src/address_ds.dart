import 'package:rxdart/rxdart.dart';
import 'package:flutter_pretest/model/address.dart';
import 'package:http/http.dart' as http;

class AddressDs {
  final String _baseUrl = 'https://private-b9a758-candidattest.apiary-mock.com/';

  Future<List<Address>> getAddress() async {
    List<Address> address = <Address>[];

    final url = Uri.parse('${_baseUrl}/address');

    http.Response response = await http.get(url);

    if (response.statusCode == 200) {
      address.addAll(AddressListFromJson(response.body));
      return address;
    } else {
      throw Exception("Unable to perform request!");
    }
  }
}
