import 'dart:convert';
import 'package:flutter_pretest/model/candidates.dart';
import 'package:http/http.dart' as http;

class CandidatesDs {
  final String _baseUrl =
      'https://private-b9a758-candidattest.apiary-mock.com/';

  Future<List<Candidates>> getPosts() async {
    List<Candidates> candidates = <Candidates>[];

    final url = Uri.parse('${_baseUrl}/candidates');

    http.Response response = await http.get(url);

    if (response.statusCode == 200) {
      candidates.addAll(candidatesListFromJson(response.body));
      return candidates;
    } else {
      throw Exception("Unable to perform request!");
    }
  }
}

class CandidatesApi {
  final String baseUrl;
  final Map<String, CandidatesResult> cache;
  final http.Client client;

  CandidatesApi({
    http.Client? client,
    Map<String, CandidatesResult>? cache,
    this.baseUrl = 'https://private-b9a758-candidattest.apiary-mock.com/',
  })  : client = client ?? http.Client(),
        cache = cache ?? <String, CandidatesResult>{};

  /// Search Github for repositories using the given term
  Future<CandidatesResult> search(String term) async {
    print(term);
    final cached = cache[term];
    if (cached != null) {
      return cached;
    } else {
      final result = await _fetchResults(term);

      cache[term] = result;

      return result;
    }
  }

  Future<CandidatesResult> _fetchResults(String term) async {
    final response = await client.get(Uri.parse('${baseUrl}candidates'));
    final results = json.decode(response.body);
    return CandidatesResult.fromJson(results['results']);
  }

  Future<CandidatesResult> getData() async {
    final getCandidates = await client.get(Uri.parse('${baseUrl}candidates'));
    final getBlog = await client.get(Uri.parse('${baseUrl}blogs'));

    final getEmail = await client.get(Uri.parse('${baseUrl}emails'));
    final getAddress = await client.get(Uri.parse('${baseUrl}address'));
    final getExperiences = await client.get(Uri.parse('${baseUrl}experiences'));

    final candidates = json.decode(getCandidates.body);
    final blog = json.decode(getBlog.body);
    final email = json.decode(getEmail.body);
    final address = json.decode(getAddress.body);
    final experiences = json.decode(getExperiences.body);

    List<dynamic> emailListJson = email['results'];
    List<dynamic> addressListJson = address['results'];
    List<dynamic> experiencesListJson = experiences['results'];
    List<dynamic> candidatesListJson = candidates['results'];

    var margeEmail = [];
    candidatesListJson.map((v) {
      emailListJson.map((v1) {
        var item = v;
        if (v1['id'] == v['id']) {
          item['email'] = v1['email'];
          item['phone'] = v1['phone'];
          margeEmail.add(item);
        }
      }).toList();
    }).toList();

    var margeAddress = [];
    margeEmail.map((v) {
      addressListJson.map((v1) {
        var item = v;
        if (v1['id'] == v['id']) {
          item["address"] = v1["address"];
          item["city"] = v1["city"];
          item["state"] = v1["state"];
          item["zip_code"] = v1["zip_code"];
          margeAddress.add(item);
        }
      }).toList();
    }).toList();

    var margeExperiences = [];
    margeAddress.map((v) {
      experiencesListJson.map((v1) {
        var item = v;
        if (v1['id'] == v['id']) {
          item["status"] = v1["status"];
          item["job_title"] = v1["job_title"];
          item["company_name"] = v1["company_name"];
          item["industry"] = v1["industry"];
          margeExperiences.add(item);
        }
      }).toList();
    }).toList();

    List<dynamic> candidateMergeInfo = margeExperiences;

    List<dynamic> blogListJson = blog['results'];
    List<dynamic> newMergeData = [...candidateMergeInfo, ...blogListJson];
    newMergeData.shuffle();
    final results = jsonDecode(jsonEncode(newMergeData));

    // print(v['subTitle'] == null ? 'candidates':'blogs');

    return CandidatesResult.fromJson(results);
  }
}

class CandidatesResult {
  final List<CandidatesResultItem> items;

  CandidatesResult(this.items);

  factory CandidatesResult.fromJson(dynamic json) {
    final items = (json as List).map((item) {
      return CandidatesResultItem.fromJson(item);
    }).toList(growable: false);

    return CandidatesResult(items);
  }

  bool get isPopulated => items.isNotEmpty;

  bool get isEmpty => items.isEmpty;
}
