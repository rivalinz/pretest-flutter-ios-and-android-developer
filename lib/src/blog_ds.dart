import 'package:flutter_pretest/model/blog.dart';
import 'package:http/http.dart' as http;

class BlogsDs {
  final String _baseUrl = 'https://private-b9a758-candidattest.apiary-mock.com/';

  Future<List<Blog>> getBlogs() async {
    List<Blog> blog = <Blog>[];

    final url = Uri.parse('${_baseUrl}/blog');

    http.Response response = await http.get(url);

    if (response.statusCode == 200) {
      blog.addAll(blogListFromJson(response.body));
      return blog;
    } else {
      throw Exception("Unable to perform request!");
    }
  }
}
